import Store from 'electron-store';

import { IS_TESTING, IS_DEV, DOWNLOAD_DIR_PATH } from '../constants';

export const storeDefaults = {
	// NOTE non usare nested, non sono type-check propriamente...
	downloadDir: DOWNLOAD_DIR_PATH,
	parallelsLimit: 1,
	automaticDownload: true,
	background: 'assets/backgrounds/def_yourname.jpg',
};

interface Settings {
	store: any;
	set<T>(key: string, value: T): void;
	get<T>(key: string): unknown | undefined;
	get<T>(key: string, defaultValue: T): T;
}

let store: Settings | undefined;

export default function getSettings(): Settings {
	if (!store) {
		if (IS_TESTING || IS_DEV) {
			store = {
				store: {},
				// eslint-disable-next-line @typescript-eslint/no-empty-function
				set: (...args) => {},
				get: <T>(key: string, defaultValue?: T) => defaultValue,
			};
		} else {
			store = new Store({
				defaults: storeDefaults,
				// TODO schema? serve?
			}) as Settings;
			console.info(`Loaded settings`, store.store);
		}
	}

	return store;
}
