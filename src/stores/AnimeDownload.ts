import { makeAutoObservable } from 'mobx';
import { remote } from 'electron';

import { Anime, AnimeOrigin, AnimeScraper } from '../search/types';
import { DownloadStagesCount } from '../downloader/types';

import { AnimeDownloaderStore } from './AnimeDownloaderStore';
import { EpisodeDownload } from './EpisodeDownload';

export class AnimeDownload {
	readonly id: string = '';
	readonly animeId: string = '';
	dir: string = '';
	anime?: Anime;
	episodes: EpisodeDownload[] = [];

	isFetching = false;

	private scraper: AnimeScraper;
	private store: AnimeDownloaderStore;

	constructor(opts: {
		scraper: AnimeScraper;
		dir: string;
		store: AnimeDownloaderStore;
	}) {
		this.id = opts.scraper.id;
		this.animeId = opts.scraper.id;
		this.dir = opts.dir;
		this.scraper = opts.scraper;

		this.store = opts.store;

		makeAutoObservable(this);
	}

	get title(): string {
		return this.anime?.title || this.scraper.title;
	}

	get origin(): AnimeOrigin {
		return this.anime?.origin || this.scraper.origin;
	}

	get coverUrl(): string | undefined {
		return this.scraper.coverUrl || this.anime?.coverUrl;
	}

	get stagesCount(): DownloadStagesCount {
		const stages: DownloadStagesCount = new Map();
		this.episodes
			.filter((episode) => episode.isInQueue)
			.forEach((episode) => {
				const stage = episode.stage;
				const count = stages.get(stage) || 0;
				stages.set(stage, count + 1);
			});
		return stages;
	}

	get downloadProgress(): number {
		const totalPercentage = 100 * this.episodesInDownload.length;
		const partialPercentage = this.episodesInDownload.reduce(
			(all, ep) => all + (ep.state.perc || 0),
			0,
		);
		return (partialPercentage / totalPercentage) * 100;
	}

	get isInDownload(): boolean {
		return this.episodes.some(
			(episode) => episode.isInQueue || episode.isDownloaded,
		);
	}

	get episodesInDownload(): EpisodeDownload[] {
		return this.episodes.filter(
			(episode) => episode.isInQueue || episode.isDownloaded,
		);
	}

	async fetchAnime(): Promise<void> {
		this.isFetching = true;
		try {
			const anime = await this.scraper.fetchAnime();

			anime.episodes.forEach((episode) => {
				const episodeDownload = this.episodes.find(
					(ed) => ed.id === episode.id,
				);
				if (episodeDownload) {
					episodeDownload.updateEpisodeData(episode);
				} else {
					this.episodes.push(
						new EpisodeDownload({
							anime: this,
							episode: episode,
							store: this.store,
						}),
					);
				}
			});

			this.anime = anime;
		} catch (err) {
			console.warn(err);
		} finally {
			this.isFetching = false;
		}
	}

	openDownloadDir = () => {
		remote.shell.openPath(this.dir);
	};
}
