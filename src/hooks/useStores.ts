import { StoresContext } from '../contexts';
import React from 'react';

export const useStores = () => {
	const stores = React.useContext(StoresContext);
	if (!stores) {
		throw new Error('useStores must be used within a StoreProvider.');
	}
	return stores;
};
