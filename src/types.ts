import { AnimeDownloaderStore } from './stores/AnimeDownloaderStore';
import { UIStore } from './stores/UIStore';

export interface Stores {
	ui: UIStore;
	animedl: AnimeDownloaderStore;
}
