import moment from 'moment';
import { DownloadState } from '../../downloader/types';

// Scritto ad inizio download con ffmpeg
// "Duration: 00:23:43.70" => ['00:23:43']
const VIDEO_DURATION_RE = /Duration: ([0-9:]*)\./;

// "frame=  181 fps=0.0 q=-1.0 size=    1536kB time=00:00:07.55 bitrate=1666.2kbits/s speed=14.9x"
// => ['00:00:07']
const TIME_RE = /time=\s*([0-9:]*)\./;
// => ['1536']
const SIZE_RE = /size=\s*([0-9]+)kB/;

const FROM_0 = moment('00:00:00', 'HH:mm:ss').toDate().getTime();

interface FfmpegDownloadState extends DownloadState {
	videoDuration?: number;
}

/**
 * Legge progresso da output di FFmpeg, solitamnete in `stderr` quando non è
 * possibile utilizzare Native HLS come metodo di download su youtube-dl.
 * */
export default function ytdlFfmpegOutput(
	prevState: FfmpegDownloadState,
	output: string,
): FfmpegDownloadState {
	if (output === '') {
		return prevState;
	}

	if (!prevState.videoDuration) {
		const matchDuration = output.match(VIDEO_DURATION_RE);
		if (matchDuration) {
			const [, duration] = matchDuration;

			return {
				...prevState,
				// Normalizza la durata del video
				videoDuration: moment(duration, 'HH:mm:ss').toDate().getTime() - FROM_0,
			};
		}
	}

	const state = { ...prevState };

	const matchTime = output.match(TIME_RE);
	if (matchTime && prevState.videoDuration) {
		const [, time] = matchTime;
		const timeDownloaded = moment(time, 'HH:mm:ss').toDate().getTime();
		const timeTotal = prevState.videoDuration;

		// timeDownloaded : timeTotal = x : 100
		// Normalizza la durata del video
		state.perc = Math.ceil(((timeDownloaded - FROM_0) * 100) / timeTotal);
		// console.log('perc:', state.perc, ' - time=', time, 'timeD=', timeDownloaded, 'timeT=', timeTotal);
	}

	const matchSize = output.match(SIZE_RE);
	if (matchSize) {
		const [, size] = matchSize;
		// kB => B
		state.sizeDownloaded = Number.parseInt(size, 10) * 1000;
	}

	return state;
}
