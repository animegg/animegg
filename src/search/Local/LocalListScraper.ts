import fs from 'fs-extra';

import { Anime, AnimeListScraper, AnimeOrigin } from '../types';

import { LocalAnimeScraper } from './LocalAnimeScraper';

export default class LocalListScraper implements AnimeListScraper {
	origin: AnimeOrigin;
	dbPath: string;

	constructor(origin: AnimeOrigin, dbPath: string) {
		this.origin = origin;
		this.dbPath = dbPath;
	}

	async fetchList(): Promise<LocalAnimeScraper[]> {
		if (!(await fs.pathExists(this.dbPath))) {
			throw new Error('Local JSON db file not found');
		}

		const animes: Anime[] = await fs.readJSON(this.dbPath);
		return animes.map((anime) => new LocalAnimeScraper(anime, this.origin));
	}
}
