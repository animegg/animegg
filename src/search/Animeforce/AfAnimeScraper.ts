import cheerio from 'cheerio';

import { AnimeScraper, AnimeId, AnimeOrigin, EpisodeType } from '../types';

import AfAnimePageParser from './AfAnimePageParser';
import AfSession from './AfSession';
import AfUtils from './AfUtils';

export default class AfAnimeScraper implements AnimeScraper {
	id: AnimeId;
	title: string;
	url: string;
	origin = AnimeOrigin.ANIMEFORCE;

	private session: AfSession;

	constructor(opts: { url: string; title: string; session: AfSession }) {
		this.id = AfUtils.calculateAnimeId(opts.url);
		this.url = opts.url;
		this.title = AfUtils.cleanTitle(AfUtils.fixTitle(opts.title));
		this.session = opts.session;
	}

	async fetchAnime() {
		const animePageStr = await this.session.requestRaw(
			this.url,
			'https://www.animeforce.org/lista-anime/',
		);

		const animePage = AfAnimePageParser.parse(animePageStr);

		const rawEpisodes = animePage.extractEpisodes();

		if (!rawEpisodes.length) {
			throw new Error('no stagioni o episodi trovati');
		}

		const sampleUrl = AfUtils.normalizeUrl(rawEpisodes[0].downloadUrl);

		const isDownloadSupported =
			sampleUrl.includes('animeforce.org') ||
			// Download diretti, non c'è baseurl da estrarre
			sampleUrl.includes('youtube.com');

		if (!isDownloadSupported) {
			throw new Error(
				`download non supportato "${this.title}":\n\t- ${this.url}\n\t- ${sampleUrl}`,
			);
		}

		const baseUrl = await this.findBaseUrl(sampleUrl);

		const episodes = rawEpisodes.map((afepisode, index) => {
			const { title } = afepisode;
			const normalizedUrl = AfUtils.normalizeUrl(afepisode.downloadUrl);
			const downloadUrl = AfUtils.substituteHost(normalizedUrl, baseUrl);

			return {
				id: AfUtils.calculateEpisodeId(normalizedUrl, title),
				animeId: this.id,
				title,
				index: index + 1,
				type: AfUtils.isEpisode(title) ? EpisodeType.SEASON : EpisodeType.EXTRA,
				url: downloadUrl,
			};
		});

		return {
			...animePage.extractSubberInfo(),
			// Non usare `...this.opts` perchè aggiunge animePage e altri dati inutili
			id: this.id,
			title: this.title,
			url: this.url,
			coverUrl: animePage.extractCoverUrl(),
			origin: AnimeOrigin.ANIMEFORCE,
			episodes,
		};
	}

	private async findBaseUrl(downloadUrl: string): Promise<string | undefined> {
		if (downloadUrl.includes('animeforce.org/ds016.php')) {
			return this.findBaseUrlFromAnimeforce(downloadUrl, this.url);
		}
	}

	/**
	 * Handler for Animeforce host and affiliates
	 * downloadUrl
	 * @example
	 * 	const downloadUrl = "https://www.animeforce.org/ds016.php?file=12SaiChicchanaMuneNoTokimeki";
	 * @param downloadUrl pagina di download di un episodio
	 * @param animeUrl necessaria come referrer per la request
	 */
	private async findBaseUrlFromAnimeforce(
		downloadUrl: string,
		animeUrl: string,
	): Promise<string> {
		try {
			const downloadPage = await this.session.requestRaw(downloadUrl, animeUrl);
			const $downloadPage = cheerio.load(downloadPage);

			// es. "http://www.lacumpa.org/DDL/ANIME/12SaiChicchanaMuneNoTokimeki/12SaiChicchanaMuneNoTokimeki_Ep_01_SUB_ITA.mp4"
			// Bottone del download
			const url = $downloadPage('div#wtf.button a:first-child').attr('href');

			if (!url) {
				throw new Error('nessun download url trovato');
			}

			// => "http://www.lacumpa.org/DDL/ANIME/"
			return AfUtils.extractBaseUrl(url);
		} catch (err) {
			throw new Error(`fallito get base url: ${err.message}`);
		}
	}
}
