import { isAbsoluteUrl } from '../../utils/net';
import { fnv1a } from '../../utils/string';
import { AnimeOrigin } from '../types';

export default class AfUtils {
	static normalizeUrl(url: string): string {
		if (!url) return '';

		// some url starts with '//animeforce.org...' -> adds prefix 'http:'
		// or //anime4you.altervista.org
		if (url.startsWith('//')) {
			return `http:${url}`;
		}

		// if url is NOT absolute -> prefix 'http://animeforce.org'
		if (!isAbsoluteUrl(url)) {
			return `http://www.animeforce.org${url}`;
		}

		return url;
	}

	/**
	 * Estrae la base dell'url dal link del download di un episodio.
	 * La base è utile per ricostruire i link di download degli altri episodi
	 * perchè AnimeForce carica tutti gli episodi sullo stesso host, quindi hanno
	 * tutti la stessa base, cambia solamente la parte dell'uri del video stesso.
	 * @example
	 * 	const url = "http://www.lacumpa.org/DDL/ANIME/12SaiChi...kimeki_Ep_01_SUB_ITA.mp4";
	 * 	const baseUrl = extractBaseUrl(url);
	 *  // => "http://www.lacumpa.org/DDL/ANIME/";
	 * @param url url del download di un episodio
	 * @return base dell'url, estratta dall'url del download di un episodio
	 */
	static extractBaseUrl(url: string) {
		const BASE_URL_BEGIN = '/htt';
		const BASE_URL_BEGIN_PAD = 1;
		const BASE_URL_END = '/ANIME';
		const BASE_URL_END_PAD = 7;

		const begin = url.search(BASE_URL_BEGIN) + BASE_URL_BEGIN_PAD;
		const end = url.search(BASE_URL_END) + BASE_URL_END_PAD;
		return url.substring(begin, end);
	}

	/**
	 * Pulisce il titolo da 'Sub Ita Down...' in coda.
	 */
	static cleanTitle(title: string): string {
		let newTitle = title;

		// Cut tail " Sub Ita Download & Streaming"
		const subItaPos = newTitle.search(/ sub i/i);
		if (subItaPos !== -1) {
			newTitle = newTitle.substring(0, subItaPos);
		}

		return newTitle;
	}

	/**
	 * Sistema certi titoli, come Idolm@ster, che viene codificato male
	 * e/o offuscato.
	 */
	static fixTitle(title: string) {
		// If '[email protected]', replace with the (only for now) correct title
		if (title.search(/\[email/i) !== -1) {
			return title.replace('[email protected]', 'Idolm@ster');
		}

		return title;
	}

	static substituteHost(url: string, host?: string) {
		if (!host) return url;

		const begin = url.search('=');
		const relativePath = url.substr(begin + 1, url.length);
		return `${host}${relativePath}`;
	}

	/**
	 * Dal titolo di un episodio cerca di riconoscere se è un episodio
	 * oppure un Film, OAV/OVA, extra, speciale, ecc
	 */
	static isEpisode(title: string): boolean {
		return !/(Film|Movie|OVA|OAV|Extra|Special)/.test(title);
	}

	static calculateAnimeId(url: string): string {
		// TODO use Wordpress post ID instead of variable url
		const urlHash = fnv1a(url);
		return `${AnimeOrigin.ANIMEFORCE}-${urlHash}`;
	}

	/**
	 * L'id degli episodi si basa sull'url.
	 * NOTE: è possibile che link con bit.ly o altri che puntano allo stessa
	 * cartella di download abbiamo il link tutti uguale.
	 * Andrebbe verificata la collisione durante la creazione o dopo.
	 */
	static calculateEpisodeId(downloadUrl: string, title: string): string {
		const urlHash = fnv1a(downloadUrl + title);
		return `${AnimeOrigin.ANIMEFORCE}-${urlHash}`;
	}
}
