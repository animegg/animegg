import VvvvidSession from './VvvvidSession';
import {
	AnimeOrigin,
	AnimeScraper,
	Anime,
	Episode,
	EpisodeType,
} from '../types';
import VvvvidUtils from './VvvvidUtils';

export interface VvvvidAnimeResponse {
	show_id: number;
	title: string;
	// 4 = anime?
	show_type: number;
	// 3 ?
	ondemand_type: number;
	thumbnail: string;
	// "Serie"
	show_type_name: string;
	// "HD"
	video_format: string;
}

interface VvvvidAnimeFullResponse extends VvvvidAnimeResponse {
	background_url: string;
}

interface VvvvidEpisodeResponse {
	id: number;
	/**
	 * `-1` se in lista ma non accessibile.
	 * es. se episodio è in uscita prossimamente.
	 */
	video_id: number;
	season_id: number;
	number: string;
	title: string;
	ondemand_type: number;
	thumbnail: string;
	/**
	 * `false` se in lista ma non accessibile.
	 * es. se episodio è in uscita prossimamente o bloccato.
	 */
	playable: boolean;
	expired: boolean;
}

interface VvvvidSeasonResponse {
	name: string;
	show_id: number;
	season_id: number;
	show_type: number;
	number: number;
	episodes: VvvvidEpisodeResponse[];
}

export default class VvvvidShowAnimeScraper implements AnimeScraper {
	origin = AnimeOrigin.VVVVID;
	id: string;
	url: string;
	title: string;

	private showId: number;
	private session: VvvvidSession;
	private vvvvidAnime: VvvvidAnimeResponse;
	private vvvvidAnimeFull?: VvvvidAnimeFullResponse;

	get coverUrl(): string {
		return this.vvvvidAnimeFull?.background_url || this.vvvvidAnime.thumbnail;
	}

	constructor(opts: {
		session: VvvvidSession;
		url: string;
		title: string;
		showId: number;
		vvvvidAnime: VvvvidAnimeResponse;
	}) {
		this.id = `${AnimeOrigin.VVVVID}-${opts.showId}`;
		this.url = opts.url;
		this.title = opts.title;

		this.showId = opts.showId;
		this.session = opts.session;
		this.vvvvidAnime = opts.vvvvidAnime;
	}

	async fetchAnime(): Promise<Anime> {
		try {
			const info = await this.fetchAnimeInfo();
			const seasons = await this.fetchAnimeSeasons();
			return this.exportAnime(seasons, info);
		} catch (err) {
			throw new Error(`AnimeScraper ${this.title}: ${err.message}`);
		}
	}

	async getCover(): Promise<string> {
		const vvvvidAnime = await this.fetchAnimeInfo();
		return vvvvidAnime.background_url;
	}

	/**
	 * Fetch anime info, like title, cover, plot and extra data
	 */
	private async fetchAnimeInfo(): Promise<VvvvidAnimeFullResponse> {
		if (this.vvvvidAnimeFull) {
			return this.vvvvidAnimeFull;
		}

		const connId = await this.session.login();

		try {
			// ex. https://www.vvvvid.it/vvvvid/ondemand/602/info/?conn_id=Tw_G_P3Sz7DA
			const vvvvidAnime = (await this.session.get(
				`vvvvid/ondemand/${this.showId}/info/?conn_id=${connId}`,
			)) as VvvvidAnimeFullResponse;
			this.vvvvidAnimeFull = vvvvidAnime;
			return vvvvidAnime;
		} catch (err) {
			throw new Error(`fallito fetch info: ${err.message}`);
		}
	}

	/**
	 * Fetch anime seasons and episodes
	 */
	private async fetchAnimeSeasons(): Promise<VvvvidSeasonResponse[]> {
		const connId = await this.session.login();

		try {
			return this.session.get(
				`vvvvid/ondemand/${this.showId}/seasons/?conn_id=${connId}`,
			) as Promise<VvvvidSeasonResponse[]>;
		} catch (err) {
			throw new Error(`fallito fetch seasons: ${err.message}`);
		}
	}

	private exportAnime(
		vseasons: VvvvidSeasonResponse[],
		info: VvvvidAnimeFullResponse,
	): Anime {
		const episodes = vseasons.reduce<Episode[]>((all, season) => {
			const seasonEpisodes = season.episodes
				// Filtra episodi non disponibili, se ad esempio sono in uscita prossimamente
				.filter((vep) => vep.playable !== false && vep.video_id !== -1)
				.map((vep, index) => {
					const slugTitle = VvvvidUtils.slugify(vep.title);
					const episodeId = `${AnimeOrigin.VVVVID}-${vep.video_id}`;
					const episode: Episode = {
						id: episodeId,
						animeId: this.id,
						title: vep.title,
						index: index + 1,
						url: `${this.url}/${vep.season_id}/${vep.video_id}/${slugTitle}`,
						// TODO correct type from season title
						type: EpisodeType.SEASON,
						groupTitle: season.name,
					};
					return episode;
				});

			return all.concat(seasonEpisodes);
		}, []);

		return {
			id: this.id,
			title: info.title,
			url: this.url,
			coverUrl: info.background_url,
			origin: AnimeOrigin.VVVVID,
			episodes,
		};
	}
}
