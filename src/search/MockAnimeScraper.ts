import { range } from '../utils/array';

import {
	AnimeOrigin,
	AnimeId,
	AnimeScraper,
	Episode,
	EpisodeType,
} from './types';

type MockAnimeConfig = {
	nEpisodes: number;
};

export class MockAnimeScraper implements AnimeScraper {
	origin = AnimeOrigin.UNKNOWN;
	id: AnimeId;
	title: string;
	url: string;

	config: MockAnimeConfig;

	constructor(id: string, config: MockAnimeConfig) {
		this.id = id;
		this.title = `Anime ${id}`;
		// Url non esistente, solo per dev
		this.url = `https://localhost/animegg/anime/${id}`;

		this.config = config;
	}

	async fetchAnime() {
		const episodes: Episode[] = range(this.config.nEpisodes).map((e, i) => {
			const episodeId = `${this.id}-${i}`;

			return {
				id: episodeId,
				type: EpisodeType.SEASON,
				animeId: this.id,
				title: `Episodio ${i + 1} (${episodeId})`,
				index: i,
				url: `${this.url}/video/${episodeId}.mp4`,
				seasonTitle: `Stagione ${i % 2}`,
			};
		});

		return {
			id: this.id,
			title: `Anime ${this.id}`,
			url: this.url,
			origin: this.origin,
			episodes,
		};
	}
}
