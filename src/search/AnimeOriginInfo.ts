import { AnimeOrigin } from './types';

export const ANIME_ORIGIN_INFO: {
	[x: string]: {
		name: string;
		homepageUrl?: string;
		logoUrl?: string;
	};
} = {
	[AnimeOrigin.VVVVID]: {
		name: 'VVVVID',
		homepageUrl: 'https://vvvvid.it',
	},
	[AnimeOrigin.ANIMEFORCE]: {
		name: 'AnimeForce',
		homepageUrl: 'https://ww1.animeforce.org',
	},
	[AnimeOrigin.YOUTUBE]: {
		name: 'YouTube',
		homepageUrl: 'https://www.youtube.com',
	},
	[AnimeOrigin.UNKNOWN]: {
		name: 'Origine?',
	},
};
