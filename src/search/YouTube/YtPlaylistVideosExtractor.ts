import cheerio from 'cheerio';

import YtUtils from './YtUtils';

export interface YtVideo {
	/**
	 * L'Id del video, come negli url al parametro "v"
	 */
	id: string;
	title: string;
}

const PL_VIDEO_SEL = '.pl-video a.pl-video-title-link';
const COVER_SEL = '.pl-header-thumb img';

/**
 * Estrae i video ed altre informazioni (come la cover) da una pagina
 * di una playlist di YouTube
 * NOTA: la pagina della playlist supportata è solamente quella
 * ricevuta dalla richiesta senza headers, ovvero senza dire di essere un browser
 */
export default class YtPlaylistVideosExtractor {
	private $page: cheerio.Selector;

	constructor(playlistPage: string) {
		this.$page = cheerio.load(playlistPage);
	}

	extractPlaylistVideos(): YtVideo[] {
		const elements = this.$page(PL_VIDEO_SEL).toArray();

		const videos: YtVideo[] = elements
			.map((elem) => {
				const $elem = this.$page(elem);
				const url = $elem.attr('href');
				const title = $elem.text().trim();
				const id = url ? YtUtils.extractVideoId(url) : undefined;

				return { title, id };
			})
			.filter((video): video is YtVideo => Boolean(video.title && video.id));

		return videos;
	}

	extractPlaylistThumbnailUrl(): string {
		// FIXME: selector not finding the image
		const coverUrlComplete = this.$page(COVER_SEL).attr('src') || '';
		// => https://i.ytimg.com/vi/YtDvsaLLHtI/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLDcHyGyGCcQB-66bxu_uF4mLLQjAw

		return YtUtils.cleanUrl(coverUrlComplete);
		// => https://i.ytimg.com/vi/YtDvsaLLHtI/hqdefault.jpg
	}
}
