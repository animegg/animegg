export enum AnimeOrigin {
	ANIMEFORCE = 'animeforce',
	VVVVID = 'vvvvid',
	YOUTUBE = 'youtube',
	UNKNOWN = 'unknown',
}

export enum EpisodeType {
	/** Episodio della stagione */
	SEASON = 'season',
	FILM = 'film',
	/** OVA, trailer, ecc */
	EXTRA = 'extra',
}

export enum Language {
	ITA = 'it',
	ENG = 'en',
	JAP = 'jp',
}

export type AnimeId = string;
export type EpisodeId = string;

/** Episodio di una stagione di un'anime */
export interface Episode {
	id: EpisodeId;
	animeId: AnimeId;
	title: string;
	url: string;
	index: number;
	type: EpisodeType;
	groupTitle?: string;
}

/** Anime, Film o stagione di un Anime */
export interface Anime {
	id: AnimeId;
	origin: AnimeOrigin;
	title: string;
	url: string;
	coverUrl?: string;
	// TODO inserire qua aggiunta da origin diversi come `subber` e opzionali?
	episodes: Episode[];
	// Youtube
	channelName?: string;
	channelId?: string;
	// AnimeForce and other sites with subber info
	// Release/Fansub name
	subberName?: string;
	// Release/Fansub site, url, page
	subberUrl?: string;
}

export interface AnimeScraper {
	readonly id: AnimeId;
	readonly origin: AnimeOrigin;
	readonly title: string;
	readonly url: string;
	readonly coverUrl?: string;

	fetchAnime(): Promise<Anime>;
}

export interface AnimeListScraper {
	readonly origin: AnimeOrigin;
	fetchList(): Promise<AnimeScraper[]>;
}
