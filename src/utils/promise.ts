/**
 * p-map: Map over promises concurrently.
 * Concurrency can range from `1` to `Infinity`
 * @version 3.0.0
 * @see https://github.com/sindresorhus/p-map
 */
export async function pMap<E, R>(
	iterable: Array<E>,
	mapper: (element: E, index: number) => Promise<R>,
	{ concurrency = Infinity, stopOnError = true } = {},
): Promise<Array<R>> {
	return new Promise((resolve, reject) => {
		const ret: R[] = [];
		const errors: Error[] = [];
		const iterator = iterable[Symbol.iterator]();
		let isRejected = false;
		let isIterableDone = false;
		let resolvingCount = 0;
		let currentIndex = 0;

		const next = () => {
			if (isRejected) {
				return;
			}

			const nextItem = iterator.next();
			const i = currentIndex;
			currentIndex++;

			if (nextItem.done) {
				isIterableDone = true;

				if (resolvingCount === 0) {
					if (!stopOnError && errors.length !== 0) {
						// Replaced `AggregateError`
						reject(new Error(errors.map((err) => err.message).join('\n')));
					} else {
						resolve(ret);
					}
				}

				return;
			}

			resolvingCount++;

			(async () => {
				try {
					const element = nextItem.value;
					ret[i] = await mapper(element, i);
					resolvingCount--;
					next();
				} catch (error) {
					if (stopOnError) {
						isRejected = true;
						reject(error);
					} else {
						errors.push(error);
						resolvingCount--;
						next();
					}
				}
			})();
		};

		for (let i = 0; i < concurrency; i++) {
			next();

			if (isIterableDone) {
				break;
			}
		}
	});
}

export function delay(ms: number): Promise<void> {
	return new Promise((res) => {
		setTimeout(res, ms);
	});
}
