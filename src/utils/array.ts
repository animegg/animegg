/**
 * @see https://stackoverflow.com/questions/3895478/does-javascript-have-a-method-like-range-to-generate-a-range-within-the-supp
 */
export function range(size: number, startAt = 0): ReadonlyArray<number> {
	return [...Array(size).keys()].map((i) => i + startAt);
}

/**
 * @version 1.0.1
 * @see https://github.com/sindresorhus/array-shuffle
 */
export function shuffle<T>(arr: ReadonlyArray<T>): T[] {
	let rand;
	let tmp;
	let len = arr.length;
	const ret = arr.slice();

	while (len) {
		rand = Math.floor(Math.random() * len--);
		tmp = ret[len];
		ret[len] = ret[rand];
		ret[rand] = tmp;
	}

	return ret;
}

/** Pick the value from an array. */
type PickValue<T> = T extends ReadonlyArray<any>
	? {
			[K in Extract<keyof T, number>]: PickValue<T[K]>;
	  }[number]
	: T;

/** Flatten an `ArrayLike` object in TypeScript. */
export type FlatArray<T extends ArrayLike<any>> = Array<PickValue<T[number]>>;

function $flatten<T extends ArrayLike<any>>(
	array: T,
	result: FlatArray<T>,
): void {
	for (let i = 0; i < array.length; i++) {
		const value = array[i];

		if (Array.isArray(value)) {
			$flatten(value as any, result);
		} else {
			result.push(value);
		}
	}
}

/**
 * Flatten an array indefinitely.
 * @see https://github.com/blakeembrey/array-flatten
 * @version 3.0.0
 */
export function flatten<T extends ArrayLike<any>>(array: T): FlatArray<T> {
	const result: FlatArray<T> = [];
	$flatten<T>(array, result);
	return result;
}

type GroupByValue<T> = { [key: string]: T[] };

/**
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce#Grouping_objects_by_a_property
 * @version 1.0
 * @param objectArray
 * @param propertyGetter
 */
export function groupBy<T extends { [x: string]: any }>(
	objectArray: T[],
	propertyGetter: (t: T) => string,
): GroupByValue<T> {
	return objectArray.reduce((acc, obj) => {
		const property = propertyGetter(obj);
		let arr = acc[property];
		if (arr) {
			arr.push(obj);
		} else {
			arr = [obj];
		}
		acc[property] = arr;
		return acc;
	}, {} as GroupByValue<T>);
}
