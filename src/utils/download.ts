import { DownloadStage, TerminalDownloadStage } from '../downloader/types';

export function isTerminalStage(
	stage: DownloadStage,
): stage is TerminalDownloadStage {
	return (
		stage === DownloadStage.COMPLETED ||
		stage === DownloadStage.STOPPED ||
		stage === DownloadStage.FAILED
	);
}

export function isDownloadingStage(
	stage: DownloadStage,
): stage is DownloadStage.DOWNLOADING {
	return stage === DownloadStage.DOWNLOADING;
}
