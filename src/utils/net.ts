import realUserAgent from 'real-user-agent';

/**
 * @version 3.0.3
 * @see https://github.com/sindresorhus/is-absolute-url/blob/master/index.js
 * @param url
 */
export const isAbsoluteUrl = (url: string) => {
	// Don't match Windows paths `c:\`
	if (/^[a-zA-Z]:\\/.test(url)) {
		return false;
	}

	// Scheme: https://tools.ietf.org/html/rfc3986#section-3.1
	// Absolute URL: https://tools.ietf.org/html/rfc3986#section-4.3
	return /^[a-zA-Z][a-zA-Z\d+\-.]*:/.test(url);
};

let userAgent =
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36';
export const getUserAgent = (): string => {
	realUserAgent().then((ua) => {
		userAgent = ua;
	});
	return userAgent;
};
