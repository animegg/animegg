import React from 'react';
import { IoIosFolderOpen } from 'react-icons/io';
import classnames from 'classnames';
import { observer } from 'mobx-react';

import { useStores } from '../hooks/useStores';

import { ToggleButton } from './Button';

export const Settings: React.FC = observer(() => {
	const { animedl, ui } = useStores();

	const handleChangeLimit = (event: React.FormEvent<HTMLInputElement>) => {
		const limit = Number(event.currentTarget.value);
		animedl.setParallelsLimit(limit);
	};

	return (
		<div>
			<h1 className="text-4xl font-thin">Impostazioni</h1>
			<SettingEntry
				title="Cartella download"
				description="La cartella dove verranno salvati i tuoi download di anime ed episodi. Ogni anime avrà una sottocartella con il titolo dell'anime ed
				all'interno troverai gli episodi scaricati."
			>
				<div className="flex flex-row space-x-2 items-center">
					<input
						className="flex-grow py-1 px-2 border-1 border-solid border-gray-400 rounded-sm"
						type="text"
						onDoubleClick={animedl.selectDownloadDir}
						readOnly={true}
						value={animedl.downloadDir}
					/>
					<button
						className="button button-primary"
						onClick={animedl.selectDownloadDir}
						title="Cambia la cartella dove salvare i download"
					>
						Cambia&nbsp;
						<IoIosFolderOpen />
					</button>
				</div>
			</SettingEntry>
			<SettingEntry
				title="Download automatico"
				description="Scarica automaticamente il download successivo al completamento del download corrente. Per evitare di sovraccaricare i siti verrà
				scaricato un download per volta."
				rightAction={
					<ToggleButton
						value={animedl.automatic}
						onToggle={animedl.toggleAutomatic}
					/>
				}
			/>
			<SettingEntry
				last={true}
				title="Download paralleli"
				description="Limite di episodi da scaricare contemporaneamente. Il limite va da 1 a 10."
				rightAction={
					<input
						className="border-1 border-solid border-gray-400 rounded-md p-1 pl-3 text-base"
						type="number"
						value={animedl.parallelDownloadsLimit}
						min={1}
						max={10}
						step={1}
						onChange={handleChangeLimit}
					/>
				}
			/>
			{/* TODO: <SettingEntry
				last={true}
				title="Sorgenti anime"
				description="Abilita e disabilita le sorgenti da cui reperire la lista di episodi"
			></SettingEntry> */}
			<hr />
			<div className="text-right mt-4">
				<button
					className="button button-secondary float-right"
					onClick={ui.openDevTools}
				>
					Apri DevTools
				</button>
			</div>
		</div>
	);
});

const SettingEntry: React.FC<{
	title: string;
	description: string;
	bottomAction?: React.ReactNode;
	rightAction?: React.ReactNode;
	last?: boolean;
}> = (props) => {
	return (
		<div
			className={classnames(
				'flex flex-col space-y-2 pb-4',
				!props.last && 'border-b-1 border-solid border-gray-400 mb-4',
			)}
		>
			<div>
				{props.rightAction && (
					<div className="inline-flex float-right">{props.rightAction}</div>
				)}
				<h2 className="text-2xl">{props.title}</h2>
			</div>
			<p>{props.description}</p>
			{props.children && <div>{props.children}</div>}
		</div>
	);
};
