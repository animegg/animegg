import { observer } from 'mobx-react';
import React from 'react';
import { IoIosArrowUp, IoIosArrowDown } from 'react-icons/io';

import { DownloadStagesCount, DownloadStage } from '../downloader/types';
import { useStores } from '../hooks/useStores';

import { StagesCount } from './StagesCount';

export const DownloaderBar: React.FC = observer(() => {
	const { ui, animedl } = useStores();
	const totalStagesCount = animedl.stagesCount;
	const isExpanded = ui.showDownloader;

	return (
		<div
			className="flex h-16 py-2 px-2 select-none bg-primary justify-between text-white z-30"
			onClick={() => ui.toggleDownloader()}
		>
			<div className="flex flex-col justify-around">
				<div className="flex space-x-2">
					<span className="text-sm font-hairline">DOWNLOADER</span>
					<StagesCount stagesCount={totalStagesCount} />
				</div>
				<span>{sumupDownloadStages(totalStagesCount)}</span>
			</div>
			<div className="p-1">
				<button
					className="button button-primary h-full text-2xl shadow-md border-solid border-1 border-primary-light rounded-md"
					title="Mostra/Nascondi il downloader. Consiglio: puoi cliccare anche su tutta la barra"
				>
					{isExpanded ? <IoIosArrowDown /> : <IoIosArrowUp />}
				</button>
			</div>
		</div>
	);
});

function sumupDownloadStages(stagesCount: DownloadStagesCount) {
	const hasReady = !!stagesCount.get(DownloadStage.READY);
	const hasDownloading = !!stagesCount.get(DownloadStage.DOWNLOADING);
	const hasCompleted = !!stagesCount.get(DownloadStage.COMPLETED);

	let msg = '';

	if (!stagesCount.size) {
		msg = 'Nessun anime in download, aggiungili dalla ricerca.';
	} else if (hasCompleted && !hasDownloading && !hasReady) {
		msg = 'Tutti i download completati. ';
	} else if (hasReady && !hasDownloading) {
		msg = 'Ci sono dei download in coda. ';
	} else if (hasDownloading) {
		msg = 'Download in corso... ';
	}

	if (stagesCount.get(DownloadStage.FAILED)) {
		msg += 'Alcuni download sono falliti. ';
	}

	if (stagesCount.get(DownloadStage.STOPPED)) {
		msg += 'Alcuni download sono stati fermati. ';
	}

	return msg;
}
