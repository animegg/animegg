import React from 'react';
import classNames from 'classnames';
import { BsToggleOn, BsToggleOff } from 'react-icons/bs';

interface Props {
	value: boolean;
	className?: string;
	onToggle: (toggle: boolean) => void;
}

export const ToggleButton: React.FC<Props> = (props: Props) => {
	const onClick = () => {
		props.onToggle(!props.value);
	};

	return (
		<button
			className={classNames(
				'button p-0 border-0 text-3xl',
				props.value && 'text-primary',
				props.className,
			)}
			onClick={onClick}
		>
			{props.value ? <BsToggleOn /> : <BsToggleOff />}
		</button>
	);
};
