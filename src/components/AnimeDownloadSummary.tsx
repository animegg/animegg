import React from 'react';
import { IoMdArrowForward } from 'react-icons/io';
import { observer } from 'mobx-react';
import { useHistory } from 'react-router';

import { AnimeDownload } from '../stores/AnimeDownload';

import ProgressBar from './ProgressBar';
import { AnimeDownloadActions } from './AnimeDownloadActions';
import { StagesCount } from './StagesCount';

type Props = {
	animeDownload: AnimeDownload;
};

export const AnimeDownloadSummary: React.FC<Props> = observer((props) => {
	const history = useHistory();
	const { animeDownload } = props;

	const goAnimePage = () => {
		history.push(`/anime/${animeDownload.animeId}`);
	};

	return (
		<div className="flex space-x-2 items-center">
			<div className="flex-grow">
				<div>
					<a
						title="Apri l'anime e visualizza gli episodi in download"
						onClick={goAnimePage}
					>
						<strong>{animeDownload.title}</strong>
					</a>
				</div>
				<div>
					<ProgressBar
						value={animeDownload.downloadProgress}
						showPercentage={true}
					/>
				</div>
				<div className="mt-1">
					<StagesCount stagesCount={animeDownload.stagesCount} />
				</div>
			</div>
			<div className="flex flex-shrink-0 items-center flex-no-wrap space-x-2">
				<AnimeDownloadActions animeId={animeDownload.animeId} />
				<button
					className="button h-8 w-8 p-0"
					title="Apri l'anime e visualizza gli episodi in download"
					onClick={goAnimePage}
				>
					<IoMdArrowForward size="20" />
				</button>
			</div>
		</div>
	);
});
