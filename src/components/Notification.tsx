import React from 'react';
import classNames from 'classnames';

interface Props {
	level?: 'warning' | 'info';
	className?: string;
	onClose?: () => void;
}

const levelStyle = {
	warning: 'bg-warning text-gray-800',
	info: 'bg-warning text-gray-800',
};

export const Notification: React.FC<Props> = (props) => {
	return (
		<div
			className={classNames(
				'px-4 py-3 rounded-lg bg-white shadow-sm text-gray-800',
				props.level && levelStyle[props.level],
				props.className,
			)}
		>
			{props.onClose && <button className="delete" />}
			{props.children}
		</div>
	);
};
