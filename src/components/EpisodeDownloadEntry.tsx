import React, { useEffect } from 'react';
import { observer } from 'mobx-react';
import { IoIosPlay, IoMdArrowDown } from 'react-icons/io';

import { isDownloadingStage } from '../utils/download';
import { EpisodeDownload } from '../stores/EpisodeDownload';
import { Episode, Anime } from '../search/types';
import { useStores } from '../hooks/useStores';

import SmartInterval from './SmartInterval';
import DownloadStateProgress from './DownloadStateProgress';
import EpisodeDownloadActions from './EpisodeDownloadActions';
import ExternalLink from './ExternalLink';

interface Props {
	episode: Episode;
	anime: Anime;
}

export const EpisodeDownloadEntry: React.FC<Props> = observer((props) => {
	const { animedl } = useStores();
	const { episode } = props;

	const download = animedl.queue.get(episode.id);

	if (!download) {
		return <EpisodeQueueable {...props} />;
	}

	return <EpisodeQueued {...props} download={download} />;
});

const EpisodeQueueable: React.FC<Props> = observer((props) => {
	const { animedl } = useStores();
	const { anime, episode } = props;
	const { title, url, index } = props.episode;

	const addToDownload = () => {
		animedl.addEpisodesToDownload(anime, [episode.id]);
	};

	return (
		<div className="flex w-full justify-between space-x-2 py-2">
			<div className="flex flex-grow items-center">
				<EpisodeHeader title={title} index={index} url={url} streamUrl={url} />
			</div>
			<div>
				<button
					className="button button-primary"
					onClick={addToDownload}
					title="Aggiunge l'episodio ai download per scaricarlo"
				>
					Scarica <IoMdArrowDown />
				</button>
			</div>
		</div>
	);
});

const EpisodeQueued: React.FC<
	Props & {
		download: EpisodeDownload;
	}
> = observer((props) => {
	const { animedl } = useStores();

	const { download, episode } = props;
	const { title, url, index } = props.episode;

	const isDownloading = isDownloadingStage(download.stage);

	useEffect(() => {
		if (isDownloading) {
			download.refresh();
		}
	}, [download, isDownloading]);

	const removeFromDownload = () => {
		animedl.removeEpisodesFromDownload([episode.id]);
	};

	return (
		<div className="flex w-full justify-between">
			<div className="flex flex-col flex-grow">
				<EpisodeHeader title={title} index={index} url={url} streamUrl={url} />
				<DownloadStateProgress {...download.state} />
			</div>
			<div className="w-40 self-center">
				<EpisodeDownloadActions
					stage={download.stage}
					onRemove={removeFromDownload}
					onStart={() => download.start()}
					onStop={() => download.stop()}
					onOpen={() => download.openFile()}
				/>
			</div>
			<SmartInterval
				run={isDownloading}
				execute={() => download.refresh()}
				ms={1000}
			/>
		</div>
	);
});

const EpisodeHeader: React.FC<{
	title: string;
	streamUrl?: string;
	url?: string;
	index?: number;
}> = (props) => {
	const handleCopyLink = () => {
		navigator.clipboard.writeText(props.url || '');
	};

	return (
		<div className="flex flex-row flex-grow relative hover-parent">
			{props.index != null && (
				<span className="select-none text-gray-600 text-right mr-2">
					{String(props.index).padStart(2, '0')}.
				</span>
			)}
			<span className="mr-2">{props.title}</span>
			<div className="hover-child text-gray-600 px-1 absolute right-0 top-0">
				{props.streamUrl && (
					<ExternalLink href={props.streamUrl}>
						[stream
						<IoIosPlay className="inline" />]
					</ExternalLink>
				)}
				{props.url && <a onClick={handleCopyLink}>[copia link]</a>}
			</div>
		</div>
	);
};
