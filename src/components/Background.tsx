import { observer } from 'mobx-react';
import React from 'react';

import { IS_WINDOWS } from '../constants';
import { useStores } from '../hooks/useStores';

function pathAsUri(path: string) {
	return !IS_WINDOWS ? path : path.replace(/\\/g, '/');
}

export const Background: React.FC = observer(() => {
	const { ui } = useStores();

	const imageUrl = pathAsUri(ui.background);
	const nextImageUrl = pathAsUri(ui.nextBackground);

	// Caricare il prossimo background: fixa flash al caricamento dell'immagine
	const nextWarmup = <img src={nextImageUrl} style={{ display: 'none' }} />;

	return (
		<div className="absolute top-0 left-0 w-screen h-screen -z-1 bg-gray-300">
			<div
				className="bg-cover bg-center absolute top-0 left-0 h-full w-full"
				style={{ backgroundImage: `url('${imageUrl}')` }}
			/>
			<div className="bg-overlay-title absolute top-0 left-0 h-full w-full" />
			{nextWarmup}
		</div>
	);
});
