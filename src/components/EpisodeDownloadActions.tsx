import React from 'react';
import {
	IoIosPlay,
	IoIosSquare,
	IoMdArrowDown,
	IoIosTrash,
	IoMdRefresh,
} from 'react-icons/io';

import { DownloadStage } from '../downloader/types';
import { isDownloadingStage } from '../utils/download';

interface EpisodeDownloadActionsProps {
	stage: DownloadStage;
	onStart: () => void;
	onStop: () => void;
	onRemove: () => void;
	onOpen?: () => void;
}

export default class EpisodeDownloadActions extends React.PureComponent<
	EpisodeDownloadActionsProps
> {
	render() {
		const { stage } = this.props;

		let actionDx;
		let actionSx;

		if (stage === DownloadStage.READY) {
			actionSx = this.renderStartButton();
			actionDx = this.renderRemoveButton();
		} else if (isDownloadingStage(stage)) {
			actionSx = (
				<button
					className="button"
					onClick={this.props.onStop}
					title="Ferma il download in corso"
				>
					Ferma <IoIosSquare />
				</button>
			);
			actionDx = this.renderRemoveButton();
		} else if (stage === DownloadStage.COMPLETED) {
			actionSx = (
				<button
					className="button button-success"
					onClick={this.props.onOpen}
					title="Apre il video scaricato"
					disabled={!this.props.onOpen}
				>
					Apri <IoIosPlay />
				</button>
			);
			actionDx = this.renderRemoveButton();
		} else if (stage === DownloadStage.STOPPED) {
			actionSx = this.renderStartButton();
			actionDx = this.renderRemoveButton();
		} else {
			// DownloadStage.FAILED
			actionSx = (
				<button
					className="button button-warning"
					onClick={this.props.onStart}
					title="Prova a riscaricare il download; è possibile che il download non sia disponibile temporaneamente"
				>
					Riprova <IoMdRefresh />
				</button>
			);
			actionDx = this.renderRemoveButton();
		}

		return (
			<div className="flex flex-shrink-0 flex-no-wrap justify-end space-x-2">
				{actionSx}
				{actionDx}
			</div>
		);
	}

	renderRemoveButton() {
		return (
			<button
				className="button"
				onClick={this.props.onRemove}
				title="Ferma il download, se in corso, e lo rimuove dalla coda, MA non cancella il file"
			>
				<IoIosTrash />
			</button>
		);
	}

	renderStartButton() {
		return (
			<button
				className="button"
				onClick={this.props.onStart}
				title="Avvia subito il download, senza aspettare il downloader automatico"
			>
				Avvia <IoMdArrowDown />
			</button>
		);
	}
}
