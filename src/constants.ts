import electron from 'electron';
import is from 'electron-is';
import path from 'path';
import os from 'os';
import pkgjson from '../package.json';

let app: electron.App | undefined;

if (is.renderer()) {
	app = electron.remote.app;
} else {
	app = electron.app;
}

export const IS_TESTING = process.env.NODE_ENV === 'testing';
export const IS_DEV = process.env.NODE_ENV === 'development';
export const IS_DEBUG = process.env.DEBUG === '1' || IS_DEV;
export const IS_RELEASE = app && app.isPackaged && true;

export const IS_WINDOWS = process.platform === 'win32';

export const MOCK_DOWNLOAD = false;
// export const MOCK_DOWNLOAD = true;
export const MOCK_SEARCH = false;
// export const MOCK_SEARCH = true;

export const APP_NAME = pkgjson.productName;
export const APP_VERSION = pkgjson.version;

const appPath = app?.getAppPath() || process.cwd();
// IS_RELEASE && app ? path.join(app.getAppPath(), 'dist') : process.cwd();

export const ASSETS_DIR_PATH = path.resolve(appPath, '..', 'assets');
export const BIN_DIR = path.resolve(ASSETS_DIR_PATH, 'bin');
export const DOWNLOAD_DIR_PATH = path.resolve(
	app?.getPath('downloads') || os.tmpdir(),
	APP_NAME,
);
