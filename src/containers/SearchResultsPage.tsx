import React from 'react';
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router';
import { observer } from 'mobx-react';

import { useStores } from '../hooks/useStores';
import { AnimeSearchResult } from '../stores/AnimeDownloaderStore';
import { ANIME_ORIGIN_INFO } from '../search/AnimeOriginInfo';

import CoverImage from '../components/CoverImage';
import { Notification } from '../components/Notification';
import ScrollableContent from '../components/ScrollableContent';

export const SearchResultsPage: React.FC = observer(() => {
	const { animedl } = useStores();
	const location = useLocation();

	const { query, results } = animedl.lastSearch;

	if (!results.length) {
		return (
			<div className="flex flex-col flex-grow h-full">
				<EmptySearchResults query={query} />
			</div>
		);
	}

	const title = query
		? `Risultati per: ${query}`
		: 'Nessun risultato, eccoti alcuni anime a caso';

	return (
		<div className="flex flex-col flex-grow h-full">
			<ScrollableContent
				className="mr-3"
				scrollRestoreKey={`results-${location.key}`}
			>
				<h1 className="text-2xl mx-3">{title}</h1>
				<div className="flex-grow grid gap-2 grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 pl-3 py-2">
					{results.map((result) => (
						<SearchResultCard key={result.id} result={result} />
					))}
				</div>
			</ScrollableContent>
		</div>
	);
});

const SearchResultCard: React.FC<{ result: AnimeSearchResult }> = (props) => {
	const { result } = props;

	return (
		<Link
			className="h-64 relative shadow-sm border-transparent border-solid border-4 hover:border-primary"
			to={`/anime/${result.id}`}
		>
			<CoverImage
				src={result.coverUrl}
				className="object-cover object-top h-full w-full"
			/>
			<div className="absolute inset-x-0 bottom-0 px-4 py-3 bg-white-overlay">
				<span className="tag float-right">
					{ANIME_ORIGIN_INFO[result.origin].name}
				</span>
				<strong>{result.title}</strong>
			</div>
		</Link>
	);
};

const EmptySearchResults: React.FC<{ query?: string }> = (props) => {
	const hasQuery = props.query && props.query !== '';

	return (
		<div className="flex flex-grow items-center justify-center">
			<Notification className="text-center">
				<p className="text-4xl">
					Nessun risultato
					{hasQuery && (
						<span>
							{' '}
							per
							<br />"{props.query}"
						</span>
					)}
					.
				</p>
				<p>
					Prova a cercare l'anime per il titolo in italiano, inglese o in
					giapponese.
				</p>
			</Notification>
		</div>
	);
};
