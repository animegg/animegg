import test from 'ava';
import VvvvidUtils from '../../../src/search/Vvvvid/VvvvidUtils';

test('slugify - replace accents', t => {
	t.is(VvvvidUtils.slugify('test à'), 'test-a');
});

test('slugify - replace spaces in between and around words', t => {
	t.is(VvvvidUtils.slugify('test  - a'), 'test-a');
	t.is(VvvvidUtils.slugify(' test a'), 'test-a');
});
