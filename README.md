<div align="center">
<img alt="AnimeGG App" width="50%" src="./assets/images/logo.png">
<h1>AnimeGG</h1>
<p>Cerca e scarica anime sub e dub italiani</p>
</div>

## Features

- Indicizzazione degli anime dalle seguenti fonti:
  - [VVVVID](https://www.vvvvid.it) (150+)
  - [AnimeForce](https://ww1.animeforce.org/) (1000+)
  - YouTube: [Yamato Entertainment](https://www.youtube.com/user/YamatoEntertainment/) (40+)
- Ricerca fuzzy per titolo, quando sai cosa cerchi
- Ricerca a random, con immagini degli anime per lasciarsi ispirare
- Stream degli episodi (tramite apertura del link)
- Copia del link dell'episodio
- Download degli episodi supportati da [youtube-dl](https://github.com/rg3/youtube-dl)
- Gestione automatica della coda di download
- Download paralleli

## Installazione

Download precedente release: [Thread ufficiale AnimeGG su Gigagame](https://gigagame.forumcommunity.net/?t=58544904)

Le nuove release verranno rese disponibili su https://gitlab.com/animegg/animegg/-/releases

## Changelog

Vedere [CHANGELOG.md](./CHANGELOG.md).

## Development

### Quick start

1. Node.js 12+ e:
   - Windows: [Microsoft Visual C++ 2010 Redistributable Package (x86)](https://www.microsoft.com/en-US/download/details.aspx?id=5555) per youtube-dl
   - Linux/macOS: `wine` (32 bit) per utilizzare youtube-dl
2. `npm install`
   - la prima volta verranno scaricati youtube-dl ed ffmpeg nella cartella `assets/bin`
3. `npm run build:dev`: Avvia il bundler e dev server con webpack
4. Su un'altro terminale avvia l'app con `npm start`

### Packaging

```bash
npm run release
```

Produce degli eseguibili dell'app:

- Windows: portable e installer (32 bit)
- Mac: .dmg con .app (64 bit)
- Linux: TODO

### Testing

Linting, type-checking e unit tests:

```bash
npm test
```

## Contributing

Vedere [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

![GNU GPLv3 logo](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)

AnimeGG - Cerca e scarica anime sub e dub italiani
Copyright (C) 2016 Sonnhy <animegg@protonmail.com>, Gigagame (gigagame.forumcommunity.net)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

The app has been developer for hobby and fun, not for profit in mind. For this very reasons no contributors can be considered liable for any misuse of the app.

A copy of the full license can be found at [LICENSE.txt](./LICENSE.txt) in this repo and is included in the release executable.

The authors of this software recognize that most technologies can be used for lawful and unlawful purposes. The authors request that this software not be used to violate any local laws. The authors recognize that this is public domain code, that they have no real powerto dictate how it is used other than a polite non-binding request,and therefore that they cannot be held responsible for what anyone chooses to do with it.
